import React, { useState } from "react";
import { Link } from "react-router-dom";


export default function PageButtons() {
    let query = window.location.search;
    let currentPage = parseInt(query.split("=")[1]);
    const [page, setPage] = useState(currentPage);
    let nextPageURL = "?page=" + String(currentPage+1);

    const handleClick = () => {
	setPage(page + 1);
	console.log(page);
    }



    return (
	    <>
	    <button onClick={handleClick}>{page} Next Page</button>
	    </>
    );
}

import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

export default function GetUpdates() {
  const [update, setUpdate] = useState([]);
    const baseURL = "https://api.wyatthorton.xyz/";
    let query = window.location.search;
    let currentPage = 0;

    if (query !== '') {
	currentPage = parseInt(query.split("=")[1]);
    }

    // can handle next page going too far if
    // update's last element is empty
    // you know the next page is empty
    // doesn't work if last page is full

    const [page, setPage] = useState(currentPage);
    const PageButtons = () => {
    let nextPageURL = "?page=" + String(currentPage+1);
    let prevPageURL = "?page=" + String(currentPage-1);

    const handleClickNext = () => {
	setPage(page + 1);
	console.log(page);
    }

    const handleClickPrev = () => {
	setPage(page - 1);
	console.log(page);
    }

    return (
	    <>
	    <div className="">
	    <button className="absolute left-2"onClick={handleClickPrev}><Link to={prevPageURL}>Prev</Link></button>
	    <button className="absolute right-2" onClick={handleClickNext}><Link to={nextPageURL}>Next</Link></button>
	    </div>
	    </>
    );
}

  const getUpdate = async () => {
    fetch(baseURL +window.location.search )
      .then((res) => res.json())
      .then((res) => {
        console.log(res);
        //res.reverse();
        setUpdate(res);
      });
  };

  useEffect(() => {
    const update = async () => {
      await getUpdate();
    };
    update();
  }, [page]);



  return (
    <>
      <div className="relative">
            <div className="grid gap-4 grid-cols-9 grid-rows-12 border-b-4 border-slate-400 mb-2 mt-1">
              <div className="col-span-1">Name</div>
              <div className="col-span-4">Description</div>
              <div className="col-span-2">Date</div>
              <div className="col-span-2">Time</div>
	  </div>
        {update.map((item) => (
          <>
            <div className="grid gap-x-8 grid-cols-9 grid-rows-12 border-b-2">
              <div className="col-span-1">{item.name}</div>
              <div className="col-span-4">{item.description}</div>
              <div className="col-span-2">{item.date}</div>
              <div className="col-span-2">{item.time}</div>
            </div>
          </>
        ))}

		{PageButtons()}
      </div>
    </>
  );
}

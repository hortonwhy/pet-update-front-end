import "./App.css";
import GetUpdates from "./pages/getUpdates.js";
import PageButtons from "./pages/pageButtons.js";

function App() {
  return (
    <div className="md:w-4/5 lg:w-3/5 m-auto flex">
	  <GetUpdates />
    </div>
  );
}

export default App;
